About:

The log_leadership_management/ directory contains a sample upload script from 2-13-2015.  
The code connects to Google and Tumblr using the credentials in google_authfile and pa_tumblr.
Then it connects to the Google spreadsheet Tumblr blog specified in columns 1 and 2, respectively,
of the first row of the CONFIG file.  The spreadsheet rows specified in column 2 of CONFIG are
tranferred to Tumblr in the form of draft blog posts.  Afterward, the number of draft posts on
all of Mike's connected Tumblr blogs are counted and printed, so the user can check the right 
number of draft posts have been uploaded. 