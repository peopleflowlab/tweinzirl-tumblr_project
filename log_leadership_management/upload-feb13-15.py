#!/usr/bin/env python

#useful tutorial links
#https://www.mattcutts.com/blog/write-google-spreadsheet-from-python/
#http://james-says.blogspot.co.uk/2007/07/beginners-guide-for-google-spreadsheet.html

import gdata.spreadsheet.service

import pytumblr
import yaml
import os


#read CONFIG file for spreadsheet key, blog
fin = open('CONFIG')
lines = fin.readlines()
spreadsheet_key, blog = lines[0].split()
ROWSTART, ROWEND = lines[1].split()
ROWSTART, ROWEND = int(ROWSTART), int(ROWEND) #row numbers as read from the spreadsheet
fin.close()

fin = open('../google_authfile')
authkey,authps = fin.readlines()[0].split()
fin.close()

gd_client = gdata.spreadsheet.service.SpreadsheetsService()
gd_client.email = authkey
gd_client.password = authps
gd_client.ProgrammaticLogin()

worksheet_feed = gd_client.GetWorksheetsFeed(spreadsheet_key)

#automatically find worksheet key for worksheet number specified in worksheet_target
worksheet_key=''
worksheet_target=1 #assume the first worksheet is the one of interest
worksheet_count=0
for worksheet in worksheet_feed.entry: 
    worksheet_count+=1
    if worksheet_target == worksheet_count:
        worksheet_key = worksheet.id.text.rsplit('/', 1)[1] #use this to get worksheet_key

gd_client = gdata.spreadsheet.service.SpreadsheetsService(spreadsheet_key, worksheet_key)
gd_client.email = authkey
gd_client.password = authps
gd_client.ProgrammaticLogin()

#initiate tumblr connection
yaml_path = '../pa_tumblr'
yaml_file = open(yaml_path, "r")
tokens = yaml.safe_load(yaml_file)
yaml_file.close()

client = pytumblr.TumblrRestClient(
tokens['consumer_key'],
tokens['consumer_secret'],
tokens['oauth_token'],
tokens['oauth_token_secret']
)

for row in range(ROWSTART,ROWEND+1,1): #up through row 109 

    print row

    title = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC1'%row).content.text
    body = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC2'%row).content.text
    tags = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC3'%row).content.text.split(',')
    customurl = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC4'%row).content.text
    source = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC5'%row).content.text
    date = gd_client.GetCellsFeed(spreadsheet_key,worksheet_key,cell='R%dC6'%row).content.text

    print tags
    for i in range(len(tags)): #remove leading '#' on tags, not strictly necessary
        print tags[i]
        tags[i] = tags[i].strip()
        if tags[i]!='' and tags[i][0]=='#': tags[i] = tags[i].strip()[1:]
        print tags[i]
        
    print title
    print body 
    print tags 
    print customurl
    print source
    print date 
        
    #does not allow link to a source
    client.create_text(blog, state="draft", slug=customurl, title=title, body=body,tags=tags,date=date)

    print 'posted draft'

#check number of drafts uploaded
print '\n\nNow manually verrify %d drafts added to %s'%(ROWEND+1-ROWSTART,blog)
X=client.info()
for b in X['user']['blogs']: print b['url'], b['drafts']

